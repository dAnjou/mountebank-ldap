## LDAP protocol for Mountebank

## Installation

Install [Mountebank](http://www.mbtest.org/).

Install ![npm](https://img.shields.io/npm/v/mountebank-ldap?label=mountebank-ldap) package.

## Usage
Create `protocols.json` file and `imposters.json` file (examples in repo).

Then run Mountebank with these files:
```
mb --configfile imposters.json --protofile protocols.json
```
Then you can run a search query (`ldapsearch` is part of [OpenLDAP](https://www.openldap.org/) which is available [in many Linux repos](https://pkgs.org/search/?q=openldap&on=summary) and [via Homebrew](https://formulae.brew.sh/formula/openldap)):
```
ldapsearch -H ldap://localhost:3898 -x -b o=iapdir "objectclass=*"
```

## How it works
![Sequence diagram showing data flow between client, custom protocol, and Mountebank.](https://cdn.jsdelivr.net/npm/mountebank-ldap/dataflow.png)
